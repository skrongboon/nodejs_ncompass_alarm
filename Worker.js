const {spawnSync} = require('child_process');
const fs = require('fs');
const path = require('path')
const PNGImage = require('pngjs-image');
const request = require('request');
const imagePath = 'image/img.png';
const imageArchivePath = 'imageArchive/';
const ffmpegPath = 'C:/ffmpeg.exe'
let token_debug= 'vGD8KlyNbhE05FRqYwMZTAFuAez74OXzESBP68VfY03';//dev
let token= '40ijs5ZARhzidfY9Hwg5fsHgBoOPbZRClmh00xgqKUd';//ncompass tvs alarm group


let screen = {
    stream_url:""
    ,ffmpeg_path:""
    ,cmd:ffmpegPath+" -i rtmp://192.168.199.45:1935/live-lb/view1 -vframes 1 -y image/img.png"
    ,capture : function(){
    // Get ncompass screen to image file;
        let ff = spawnSync('cmd.exe',['/c',this.cmd],{timeout:5000});
        if(ff.stderr.toString().indexOf("failed to connect socket")!==-1
            || ff.error){
            log("capture error|timeout");
            this.capture();
        }
        log("Capture screen complete.");
        //return "this is screen image";
    }
    ,renameImg:function(){
        log("Renaming");
        fs.rename('image/img.png','image/'+Math.floor(Date.now()/1000)+'_img.png',function(err){
            if(err)
                log('Rename image error',err);
        });
    }
    ,remove:function(){
        //remove image older than ..
        let datenow = Math.floor(Date.now()/1000);
        fs.readdir('image',function(err,files){
            if(!err){
                for(var i =0; i<files.length-1; i++){
                    if((datenow-files[i].split("_")[0])>120){
                        log('remove ok',files[i]);
                       fs.unlink('image/'+files[i],function(err){
                            if(err)
                                log(`remove err:${err}`)
                       }); 
                    }
                }
            }
        });
    }
    ,archive:function(){
        log("Archive alarm image");
        //archive current img.png if have alarms
        fs.copyFile(imagePath,imageArchivePath+'alarm_'+Math.floor(Date.now()/1000)+'.png'
            ,(err)=>{
                if(err) log('copy file error',err);
            });
    }

}

let validator ={
    warnRGB:{
        r:239,g:133,b:25
    }
    ,normalRGB:{
        r:22,g:231,b:17
    }
    ,errorRGB:{
        r:205,g:20,b:19
    }
    ,switchRGB:{
        r:17,g:15,b:136
    }
    ,found:null
    ,imgRead:null
    ,screenLayout:null
    ,start:function(resolve=null){
        var that = this;
        this.found =null 
        this.imgRead=null 
        log('start validate')
        PNGImage.readImage(imagePath,function(err,imgRead){
            if(err){
                log("read img error:",err);
                resolve(that.found);
            }
            log("read image complete");
            that.imgRead = imgRead
            that._validate(that.screenLayout); 
            resolve(that.found);
        });
    }
    ,_validate: function(screenLayout){
        //Loop over screen layout and scan pixel
        for(const screenName in screenLayout){
            const screen = screenLayout[screenName]
            const promise =this._scanPixel(screen.start,screen.end,screenName);
        }
    }
    ,_scanPixel:function(corStart,corEnd,screenName){
        const x1= corStart[0]
            ,y1=corStart[1]
            ,x2=corEnd[0]
            ,y2=corEnd[1]

        //start scan top-to-bottom 
        for(let ver=y1;ver<y2;ver++){
            //scan left-to-right
            for(let hor=x1;hor<x2;hor++){
                //console.log(hor+','+ver)
                let R,G,B;
                R = this.imgRead.getRed(
                        this.imgRead.getIndex(hor,ver));
                G = this.imgRead.getGreen(
                        this.imgRead.getIndex(hor,ver));
                B = this.imgRead.getBlue(
                        this.imgRead.getIndex(hor,ver));

                let rgb =R+','+G+','+B;
                //find alarm and error
                let rstCheck =this._findNearestColor(null,{r:R,g:G,b:B}) 
                if(rstCheck){
                    if(this.found==null){
                        this.found = {};
                    }
                    if(this.found[screenName]==undefined){
                        this.found[screenName] = {
                            warn:0,error:0,devSwitch:0
                        } 
                    }
                    this.found[screenName].warn +=rstCheck.warn
                    this.found[screenName].error +=rstCheck.error;
                    this.found[screenName].devSwitch +=rstCheck.devSwitch;
                }

            }
        }
    }
    ,_findNearestColor:function(mc,fc){
        //Uclidean method dis = abs(r1-r2)+abs(g1-g2)+(b1-b2)
        // mc = main color, fc = found color
        mc = {
            r:25,g:228,b:23 // this is green
        }
        let thredshold = 100,distance,difR,difG,difB
        ,error=0,warn=0,devSwitch=0
        //check warn
        mc = this.warnRGB;
        difR    = Math.abs(mc.r-fc.r)
        difG    = Math.abs(mc.g-fc.g)
        difB    = Math.abs(mc.b-fc.b)
        distance = difR+difG+difB;
        if(distance<=32) warn = 1;
        //checkerror 
        mc = this.errorRGB;
        difR    = Math.abs(mc.r-fc.r)
        difG    = Math.abs(mc.g-fc.g)
        difB    = Math.abs(mc.b-fc.b)
        distance = difR+difG+difB;
        if(distance<=32) error = 1;
        //check device switch
        mc = this.switchRGB;
        difR    = Math.abs(mc.r-fc.r)
        difG    = Math.abs(mc.g-fc.g)
        difB    = Math.abs(mc.b-fc.b)
        distance = difR+difG+difB;
        if(distance<=32) devSwitch = 1;
        
        if(error || warn || devSwitch) return {error:error,warn:warn,devSwitch:devSwitch};
        else return false;
    }
}
function notify(alarmResult=null){
	log("send notify")
    //make alarm text
    let alarmText = "" 
    let confif = {};
    for(const ts in alarmResult){
        if(alarmResult[ts].warn != 0){
           alarmText += "\r\n["+ts+"] => has waring" 
        }
        if(alarmResult[ts].error != 0){
           alarmText += "\r\n["+ts+"] => has error" 
        }
        if(alarmResult[ts].devSwitch != 0){
           alarmText += "\r\n["+ts+"] => has device switched" 
        }
    }

    if(alarmResult == null){
        log("Heart beat checked!");
        // for hearth beat check app is alive;
        alarmText = "nCompass alarm : is alive (every 30 min.)";
        config = {
            method:'POST'
            ,uri:'https://notify-api.line.me/api/notify'
            ,header:{
                'connection':'keep-alive'
                ,'Content-Type':'application/x-www-form-urlencoded'
            }
            ,auth:{ 'bearer':token_debug}
            ,form:{
                message:alarmText
            }
        }
    }else{
        config = {
            method:'POST'
            ,uri:'https://notify-api.line.me/api/notify'
            ,header:{
                'connection':'keep-alive'
            }
            ,auth:{ 'bearer':token}
            ,formData:{
                message:"nCompass alarm"+alarmText
                ,imageFile:{
                    value:fs.createReadStream(imagePath)
                    ,options:{
                        filename:'img.png'
                        ,contentType:'image/png'
                    }
                }}
        }
    }

    request(config)
    .on("response",(res)=>{
        if(res.statusCode!=200){
            log(JSON.stringify(res))
            notify(alarmResult);
        }else{
            log("Sending notify complete") 
            return true
        };
    })
    .on("error",(res)=>{
        log(JSON.stringify(res))
        notify(alarmResult);
    });
}
function log(msg=null,toFile=false){
    let date = new Date() 
    ,yyyy = date.getFullYear()
    ,mm = (date.getMonth()+1<10)?"0"+(date.getMonth()+1):date.getMonth()+1
    ,dd = date.getDate()
    ,h = date.getHours()
    ,m = date.getMinutes()
    ,s = date.getSeconds()
    ,dateTxt = yyyy+"/"+mm+"/"+dd+" "+h+":"+m+":"+s
    if(toFile){

    }else{
        console.log(`${dateTxt} : ${msg}`);
    }
}

module.exports = {
    screen:screen
    ,validator:validator
    ,notify:notify
    ,log:log
};