const cron = require('node-cron');
const w = require('./worker');  
const screenLayout = {
    "DSTV:TS2":{start:[6,23],end:[318,128]}
    ,"DSTV:TS3":{start:[328,23],end:[639,125]}
    ,"DSTV:TS4":{start:[647,23],end:[955,121]}
    ,"DSTV:TS5":{start:[967,23],end:[1274,131]}
    ,"DSTV:TS6":{start:[6,199],end:[318,293]}
    ,"DSTV:TS7":{start:[328,199],end:[639,291]}
    ,"DSTV:TS8":{start:[647,199],end:[955,291]}
    ,"DSTV:TS9":{start:[967,199],end:[1274,291]}
    ,"DSTV:TS10":{start:[645,382],end:[955,476]}
    ,"C-BAND":{start:[968,382],end:[1274,476]}
    ,"RE:TS59:TS60:TS67":{start:[645,567],end:[955,660]}
    ,"REMUX:TS66":{start:[968,562],end:[1274,670]}
}
w.log('start node server');
cron.schedule('10 * * * *',function(){
    // for heart beat check 
    w.notify();
});
cron.schedule('* * * * *',function(){
    w.log("Cron activate");
    w.screen.capture();
    w.validator.screenLayout = screenLayout;
    let promise = new Promise((resolve,reject)=>{
        w.validator.start(resolve);
    })
    promise.then((foundAlarm)=>{
        w.log((foundAlarm==null)?"No Alarm":JSON.stringify(foundAlarm));
        if(foundAlarm != null){
            w.notify(foundAlarm);
            w.screen.archive();
        }else w.screen.renameImg();
		w.screen.remove();
    })
});


/* For test */
function validate(){
    
    w.validator.screenLayout = screenLayout;
    let promise = new Promise((resolve,reject)=>{
        w.validator.start(resolve);
    })
    promise.then((foundAlarm)=>{
        w.log((foundAlarm==null)?"No Alarm"
            :"Found alarm"+JSON.stringify(foundAlarm));
        if(foundAlarm != null){
            w.notify(foundAlarm);
            w.screen.archive();
        }else w.screen.renameImg();
    })
}